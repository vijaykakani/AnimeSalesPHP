<pre>
<?php
/**
 * Created by PhpStorm.
 * User: Vijay
 * Date: 30/08/2015
 * Time: 8:10 PM
 */

$file = 'Anime Sales- Distributor-Publisher Totals - Source.csv';

$set = array();

if (($handle = fopen($file, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $set[] = $data;
    }
    fclose($handle);
}

//print_r($set);

$str_series_len = $str_average_len = $str_year_len = $str_yen_len = 0;

foreach ($set as $data)
{
    if (($len = strlen($data[0])) > $str_series_len)
        $str_series_len = $len;

    if (($len = strlen($data[1])) > $str_average_len)
        $str_average_len = $len;

    if (($len = strlen($data[3])) > $str_year_len)
        $str_year_len = $len;

    if (($len = strlen(substr($data[4], 1))) > $str_yen_len)
        $str_yen_len = $len;
}

$head_data = array_shift($set);

$head_line = '| '.
    str_position($head_data[0], $str_series_len).' | '.
    str_position($head_data[1], $str_average_len).' | '.
    str_position($head_data[3], $str_year_len).' | '.
    str_position($head_data[4], $str_yen_len).' | ';
echo $head_line."\n";
echo str_position('', strlen($head_line), '-')."\n";

usort($set, "cmp");

foreach ($set as $data)
{
    $str = '| '.
        str_position($data[0], $str_series_len).' | '.
        str_position($data[1], $str_average_len).' | '.
        str_position($data[3], $str_year_len).' | '.
        str_position(substr($data[4], 1), $str_yen_len).'  |';
    echo $str."\n";
}

?>
</pre>
<?php
function str_position($str, $pos, $chr = ' ')
{
    for($i = strlen($str); $i < $pos; $i++)
        $str .= $chr;
    return $str;
}

function cmp($a, $b)
{
    $sales_a = intval(str_replace(',', '', substr($a[4], 2)));
    $sales_b = intval(str_replace(',', '', substr($b[4], 2)));

    return $sales_a < $sales_b;
}
?>