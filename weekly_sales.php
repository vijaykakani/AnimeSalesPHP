<html>
    <head>
        <title>Anime - Weekly Sales</title>
    </head>
    <body>
<?php
/**
 * Created by PhpStorm.
 * User: Vijay
 * Date: 19/10/2014
 * Time: 10:57 PM
 */

if (isset($_POST['calculate']))
{
    $datas = getdata(@$_POST['weekly_sales_txt']);

    //print_pre(print_r($datas, true));

    $index_rank_key = 0;
    $index_cost_key  = 6;
    $index_title_key = 12;

    $len_rank = $len_cost = $len_title = 0;

    foreach ($datas as $data)
    {
        if (($len = strlen($data[$index_rank_key])) > $len_rank)
            $len_rank = $len;

        if (($len = strlen($data[$index_cost_key])) > $len_cost)
            $len_cost = $len;

        if (($len = strlen($data[$index_title_key])) > $len_title)
            $len_title = $len;
    }

    $lines = "";
    foreach ($datas as $data)
    {
        $str = '| '.
            str_position($data[$index_rank_key], $len_rank).' | '.
            str_position($data[$index_cost_key], $len_cost).' | '.
            str_position($data[$index_title_key], $len_title).'  |';
        $lines .= $str."\n";
    }

    print_pre($lines);
}
?>
    <form action="" method="post">
        <textarea name="weekly_sales_txt" style="width: 100%; height: 300px;"><?=@$_POST['weekly_sales_txt']?></textarea>
        <br />
        <input name="calculate" type="submit" value="Generate" style="padding: 5px; font-size: 20px; width: 100%;" />
    </form>
    </body>
</html>
<?php
function tdrows($elements)
{
    $array = array();
    foreach ($elements as $element) {
        $array[] = trim($element->nodeValue);
    }
    return $array;
}

function getdata($contents)
{
    $DOM = new DOMDocument;
    $DOM->loadHTML($contents);

    $items = $DOM->getElementsByTagName('tr');

    $rows = array();
    $i = 0;
    foreach ($items as $node) {
        $i++;
        if ($i == 1) continue;

        //echo "ROW ".$i."\n";
        $rows[] = tdrows($node->childNodes);
    }

    return $rows;
}

function str_position($str, $pos, $chr = ' ')
{
    for($i = strlen($str); $i < $pos; $i++)
        $str .= $chr;
    return $str;
}

function print_pre($text) { print "<pre>".$text."</pre>"; }
?>