<pre>
<?php
/**
 * Created by PhpStorm.
 * User: Vijay
 * Date: 11/09/2015
 * Time: 11:38 PM
 */

$headers = array();

function tdrows($elements)
{
    $array = array();
    foreach ($elements as $element) {
        $array[] = trim($element->nodeValue);
    }
    return $array;
}

function getdata($contents)
{
    global $headers;

    $DOM = new DOMDocument;
    $DOM->loadHTML($contents);

    $items = $DOM->getElementsByTagName('tr');

    $rows = array();
    $i = 0;
    foreach ($items as $node) {
        $i++;
        if ($i == 1)
        {
            $headers = tdrows($node->childNodes);
            continue;
        }

        //echo "ROW ".$i."\n";
        $rows[] = tdrows($node->childNodes);
    }

    return $rows;
}

$datas = getdata(@$_POST['data']);

$headers = array_filter($headers);

//print_r($datas);

$index_sales_key  = 0;
$index_title_key = 14;

$series = array();
$i = 1;
foreach ($datas as $data)
    $series[] = array($i++, $data[$index_sales_key], $data[$index_title_key]);

//print_r($series);
//print_r($headers);

$len_rank = $len_sales = $len_title = 0;

foreach ($series as $story)
{
    if (($len = strlen($story[0])) > $len_rank)
        $len_rank = $len;

    if (($len = strlen($story[1])) > $len_sales)
        $len_sales = $len;

    if (($len = strlen($story[2])) > $len_title)
        $len_title = $len;
}

if (($len = strlen("Rank")) > $len_rank)
    $len_rank = $len;

if (($len = strlen($headers[0])) > $len_sales)
    $len_sales = $len;

if (($len = strlen($headers[14])) > $len_title)
    $len_title = $len;

$str = '| '.
    str_position("Rank", $len_rank).' | '.
    str_position($headers[0], $len_sales).' | '.
    str_position($headers[14], $len_title).'  |';
echo str_position("", strlen($str), "-")."\n";
echo $str."\n";
echo str_position("", strlen($str), "-")."\n";

foreach ($series as $story)
{
    $str = '| '.
        str_position($story[0], $len_rank).' | '.
        str_position($story[1], $len_sales).' | '.
        str_position($story[2], $len_title).'  |';
    echo $str."\n";
}

echo str_position("", strlen($str), "-")."\n";

?>
</pre>
<form action="" method="post">
    <textarea name="data" style="width: 100%; height: 250px;"><?=@$_POST['data']?></textarea>
    <input name="process" type="submit" style="width: 100%; font-size: 16px;" />
</form>
<?php
function str_position($str, $pos, $chr = ' ')
{
    for($i = strlen($str); $i < $pos; $i++)
        $str .= $chr;
    return $str;
}
?>