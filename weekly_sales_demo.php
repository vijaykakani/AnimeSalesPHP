<pre>
<?php
/**
 * Created by PhpStorm.
 * User: Vijay
 * Date: 11/09/2015
 * Time: 11:38 PM
 */

function tdrows($elements)
{
    $array = array();
    foreach ($elements as $element) {
        $array[] = trim($element->nodeValue);
    }
    return $array;
}

function getdata($contents)
{
    $DOM = new DOMDocument;
    $DOM->loadHTML($contents);

    $items = $DOM->getElementsByTagName('tr');

    $rows = array();
    $i = 0;
    foreach ($items as $node) {
        $i++;
        if ($i == 1) continue;

        //echo "ROW ".$i."\n";
        $rows[] = tdrows($node->childNodes);
    }

    return $rows;
}

$datas = getdata(@$_POST['data']);

print_r($datas);
?>
</pre>
<form action="" method="post">
    <textarea name="data" style="width: 100%; height: 250px;"></textarea>
    <input name="process" type="submit" style="width: 100%; font-size: 16px;" />
</form>