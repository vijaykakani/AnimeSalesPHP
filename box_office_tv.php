<pre>
<?php
/**
 * Created by PhpStorm.
 * User: Vijay
 * Date: 11/09/2015
 * Time: 11:38 PM
 */

function tdrows($elements)
{
    $array = array();
    foreach ($elements as $element) {
        $array[] = trim($element->nodeValue);
    }
    return $array;
}

function getdata($contents)
{
    $DOM = new DOMDocument;
    $DOM->loadHTML($contents);

    $items = $DOM->getElementsByTagName('tr');

    $rows = array();
    $i = 0;
    foreach ($items as $node) {
        $i++;
        if ($i == 1) continue;

        //echo "ROW ".$i."\n";
        $rows[] = tdrows($node->childNodes);
    }

    return $rows;
}

$datas = getdata(@$_POST['data']);

//print_r($datas);

$index_cost_key  = 2;
$index_title_key = 8;

$len_cost = $len_title = 0;

foreach ($datas as $data)
{
    if (($len = strlen(substr($data[$index_cost_key], 1))) > $len_cost)
        $len_cost = $len;

    if (($len = strlen($data[$index_title_key])) > $len_title)
        $len_title = $len;
}

foreach ($datas as $data)
{
    $str = '| '.
        str_position(substr($data[$index_cost_key], 1), $len_cost).' | '.
        str_position($data[$index_title_key], $len_title).'  |';
    echo $str."\n";
}

?>
</pre>
<form action="" method="post">
    <textarea name="data" style="width: 100%; height: 250px;"><?=@$_POST['data']?></textarea>
    <input name="process" type="submit" style="width: 100%; font-size: 16px;" />
</form>
<?php
function str_position($str, $pos, $chr = ' ')
{
    for($i = strlen($str); $i < $pos; $i++)
        $str .= $chr;
    return $str;
}
?>